/*
 ==============================================================================================================================================
 Name        : Citizen details
 Author      : Sadrullah Noori
 Version     : 1.0
 Description : This programme is designed to be able to store input form user and then save those inputs into a file for later use. The -------------:-programme that I have designed uses verity of functions such as printf, scanf, ‘typedef’ structure which contains memory -------------:-allocation and other techniques that I have learnt so far in my course.  ===============================================================================================================================================
 */
// these are the libraries
#include <stdio.h>
#include <stdlib.h>

#include "config.h" // auto generated

//main functions including veriaty of techniques such as char, float and init. 
typedef struct {
  unsigned id;
  int running;
  char *name;
  char *surname; 
  char *sex;
  char *DOB;
  float *height;
  char *Address;
} citizen;


citizen *make_citizen(unsigned id) {
  
  citizen *person;

// memory allocation using malloc  
  if ((person = (citizen *)malloc(sizeof(citizen))) == NULL) {
    fprintf(stderr, "Failed to allocate citizen structure!\n");
    exit(EXIT_FAILURE);
  }
  person->running = 0;
  person->id = id;
  person->name = NULL;
  person->surname = NULL;
  person->sex = NULL;
  person->DOB = NULL;
  person->height = NULL;
  person->Address = NULL;
 
 return person;
}

void free_citizen(citizen *person) {
  free(person->name);
  free(person->surname);
  free(person->sex);
  free(person->DOB);
  free(person->height);
  free(person->Address);
  free(person);
}

void run_citizen(citizen *person) {
  person->running = 1;
}
// setting user detail such as name surname etc
void set_citizen_name(citizen *person, char *name)
{
 person->name = strdup(name);
}

void set_citizen_surname(citizen *person, char *surname)
{
 person->surname = strdup(surname);
}

void set_citizen_sex(citizen *person, char *sex)
{
 person->sex = strdup(sex);
}

void set_citizen_DOB(citizen *person, char *DOB)
{
 person->DOB = strdup(DOB);
}

void set_citizen_height(citizen *person, float *height)
{
 person->height = strdup(height);
}
void set_citizen_Address(citizen *person, char *Address)
{
 person->Address = strdup(Address);
}
int is_citizen_running(citizen *person) {
  return person->running;
}
 
void getcitizen(citizen *person){
// user input allowing the users to input thier detail

  printf("\n Enter your Name: "); 
  gets(person->name); 
  printf("\n Enter your Surname: "); 
  gets(person->surname);
  printf("\n Whats your sex: "); 
  gets(person->sex);
  printf("\n Enter your DOB: "); 
  gets(person->DOB);
  printf("\n Enter your height: "); 
  gets(person->height);
  printf("\n Enter your Address: "); 
  gets(person->Address);
}


int main()
{
  citizen *person;
// texts that will appear when running the programme
  char resp;
  
  person = make_citizen(1001);
  run_citizen(person);
  set_citizen_name(person, "sadrullah");
  set_citizen_surname(person, "Noori");
  set_citizen_sex(person, "Male");
  set_citizen_DOB(person, "19/09/1997");
  set_citizen_height(person, "5/10 ft");
  set_citizen_Address(person, "118 sheaveshill avenue\nLondon, colindale\nNW9 6RY");    

if (is_citizen_running(person))
 {
    printf("Name: %s\nSurname: %s\nSex: %s\nDOB: %f\nHeight: %s\nAddress: %s\n", person->name, person->surname, person->sex, person->DOB, person->height, person->Address);
  }
  free_citizen(person);

  FILE *fptr; 
// starting the code for user input

  if((fptr = fopen("citizen.txt","a+")) == NULL)  exit(EXIT_FAILURE);
// error handling hence exit failur
	{  

	do{
  	getcitizen(person); 
  	fprintf(fptr,"\n Name: %s\nSurname:%s Sex:%s DOB:%s height:%s Address:%s",person->name, person->surname, person->sex, person->DOB,person->height, person->Address);

// giving the users another option to register more and more people  
	printf("\nOne more person (y/n)? ");
	scanf("%c" ,&resp);
	}while(resp != 'n' && resp != 'n'); 
    fclose(fptr); 
	}
return 0;
}

 





