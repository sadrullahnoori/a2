#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct {
  unsigned id;
  int running;
  char *name;
  char *surname; 
  char *sex;
  char *DOB;
  float *height;
  char *Address;
} citizen;

citizen *make_citizen(unsigned id);
void free_citizen(citizen *person);
void run_citizen(citizen *person);
void set_citizen_name(citizen *person, char *name);
void set_citizen_surname(citizen *person, char *surname);
void set_citizen_sex(citizen *person, char *sex);
void set_citizen_DOB(citizen *person, char *DOB);
void set_citizen_height(citizen *person, float *height);
void set_citizen_Address(citizen *person, char *Address);
int is_citizen_running(citizen *person);


