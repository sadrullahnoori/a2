#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "person.h"

citizen *make_citizen(unsigned id) {
  citizen *person;
  
  if ((person = (citizen *)malloc(sizeof(citizen))) == NULL) {
    fprintf(stderr, "Failed to allocate citizen structure!\n");
    exit(EXIT_FAILURE);
// function for the type of information that i want to collect

  }
  person->running = 0;
  person->id = id;
  person->name = NULL;
  person->surname = NULL;
  person->sex = NULL;
  person->DOB = NULL;
  person->height = NULL;
  person->Address = NULL;
 
 return person;
}
// code below will free the person after the mandetory information is collected
void free_citizen(citizen *person) {
  free(person->name);
  free(person->surname);
  free(person->sex);
  free(person->DOB);
  free(person->height);
  free(person->Address);
  free(person);
}

void run_citizen(citizen *person) {
  person->running = 1;
}

void set_citizen_name(citizen *person, char *name)
{
 person->name = strdup(name);
}

void set_citizen_surname(citizen *person, char *surname)
{
 person->surname = strdup(surname);
}

void set_citizen_sex(citizen *person, char *sex)
{
 person->sex = strdup(sex);
}

void set_citizen_DOB(citizen *person, char *DOB)
{
 person->DOB = strdup(DOB);
}

void set_citizen_height(citizen *person, float *height)
{
 person->height = strdup(height);
}
void set_citizen_Address(citizen *person, char *Address)
{
 person->Address = strdup(Address);
}
int is_citizen_running(citizen *person) {
  return person->running;
}

